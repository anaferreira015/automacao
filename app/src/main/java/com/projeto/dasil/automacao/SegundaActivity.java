package com.projeto.dasil.automacao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SegundaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);

        Button btEq = (Button) findViewById(R.id.btEq);
        Button btReles = (Button) findViewById(R.id.btReles);
        Button btConfig = (Button) findViewById(R.id.btConfig);

        btReles.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startTerceiraActivity();

            }
        });

        btEq.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startEquipamentosActivity();

            }
        });

        btConfig.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startConfiguracaoActivity();

            }
        });
    }

    public void startTerceiraActivity() {
        //Intents são usados para a transição entre activities
        Intent terceiraActivity = new Intent(this, TerceiraActivity.class);
        startActivity(terceiraActivity); //iniciando a tela de reles
    }

    public void startEquipamentosActivity() {
        Intent eqActivity = new Intent(this, EquipamentosActivity.class);
        startActivity(eqActivity); //inicia a tela de equipamentos
    }

    public void startConfiguracaoActivity() {
        Intent configActivity = new Intent(this, ConfiguracaoActivity.class);
        startActivity(configActivity); //inicia a tela de configurações
    }

}
