package com.projeto.dasil.automacao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ConfiguracaoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracao);

        Button btEspReles = (Button) findViewById(R.id.btEspReles);

        btEspReles.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startEditRelesActivity();

            }
        });
    }

    public void startEditRelesActivity() {

        Intent EditRelesActivity = new Intent(this, EditRelesActivity.class); //Intents são usados para a transição entre activities
        startActivity(EditRelesActivity); //iniciando a tela de editar os reles
    }
}
