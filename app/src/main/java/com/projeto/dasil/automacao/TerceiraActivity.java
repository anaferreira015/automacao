package com.projeto.dasil.automacao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class TerceiraActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terceira);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        EditText editText1 = (EditText) findViewById(R.id.editText1);
        EditText editText2 = (EditText) findViewById(R.id.editText2);
        if(bundle != null){
            String txt = bundle.getString("txt");
            editText1.setText(txt);

            String txt2 = bundle.getString("txt2");
            editText2.setText(txt2);
        }


    }


}
