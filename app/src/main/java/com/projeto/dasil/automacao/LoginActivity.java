package com.projeto.dasil.automacao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btLogin = (Button) findViewById(R.id.btLogin);

        btLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                TextView tLogin = (TextView) findViewById(R.id.tLogin);
                TextView tSenha = (TextView) findViewById(R.id.tSenha);

                String login = tLogin.getText().toString();
                String senha = tSenha.getText().toString();

                if (login.equals("Asteroblaudo")&&senha.equals("123")) {
                    alert("Login realizado!");
                    startSecondActivity();
                } else if (login.isEmpty()||senha.isEmpty()) {
                    alert("Login ou senha não informado!");
                } else {
                    alert("Login ou senha incorreto");
                }

            }
        });
    }

    public void startSecondActivity() {

        Intent secondActivity = new Intent(this, SegundaActivity.class); //Intents são usados para a transição entre activities
        startActivity(secondActivity); //iniciando a segunda tela
    }

    private void alert (String s) {

        Toast.makeText(this,s,Toast.LENGTH_LONG).show();
    }
}
