package com.projeto.dasil.automacao;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EditRelesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_reles);

        Button btSalvar = (Button) findViewById(R.id.btSalvar);
        btSalvar.setOnClickListener(oncli);
    }

    public View.OnClickListener oncli = new View.OnClickListener(){
        @Override
        public void onClick (View view) {
            Intent intent = new Intent(EditRelesActivity.this, TerceiraActivity.class);

            EditText editText1 = (EditText) findViewById(R.id.editText1);
            EditText editText2 = (EditText) findViewById(R.id.editText2);

            String txt = "";
            txt = editText1.getText().toString();
            Bundle bundle = new Bundle();
            bundle.putString("txt", txt);
            intent.putExtras(bundle);

            String txt2 = "";
            txt2 = editText2.getText().toString();
            bundle.putString("txt2", txt2);
            intent.putExtras(bundle);

            startActivity(intent);
            alert("Salvo com sucesso!");
        }

    };

    private void alert (String s) {

        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }

}
