package com.projeto.dasil.automacao;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.sql.SQLException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startLoginActivity();
            }
        },3500);
    }


    public void startLoginActivity() {

        Intent secondActivity = new Intent(this, LoginActivity.class); //Intents são usados para a transição entre activities
        startActivity(secondActivity); //iniciando a tela de login
    }
}
